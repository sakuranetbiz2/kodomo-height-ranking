<?php get_header(); ?>

  	<div class="container">
		<div class="row">
			<div class="archive col-md-9">

			<?php if ( have_posts() ) : ?>
					<?php the_archive_title( '<h2 class="page-title">', '</h2>' ); ?>
			<?php endif; ?>

			<?php if ( have_posts() ) : ?>
			  <?php while ( have_posts() ) : the_post(); ?>
				<div class="archive-list">
					<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					<?php the_excerpt(); ?>
				</div>
			  <?php endwhile; ?>
			<?php endif; ?>

			</div>

<?php get_sidebar(); ?>
			
		</div>
	</div>

<?php get_footer(); ?>