<?php get_header(); ?>

<section class="about-site">
	<div class="container">
			<h2><img src="<?php echo get_template_directory_uri(); ?>/images/about-site_title.png" class="img-responsive center-block" alt=""/></h2>
		<div class="about-site-txt">
			<ul class="check-red">
				<li>たくさん商品があって、どれを選んで良いか分からない…</li>
				<li>クチコミサイトも見るがは、信用できるかどうか分からない…</li>
				<li>アンケート調査を実施したサイトを見ても、アンケート数が少ないから…</li>
			</ul>
			<p>当サイトは、そのようなお悩みに対して、成長期のお子様をもつご父母の方々が最適な商品を選択できるように以下の点を遵守し情報を掲載しております。</p>
			<ul class="check-green">
				<li>成長期のお子様をもつ5,000人のお母さま方に調査した結果を公正にランキング化</li>
				<li>複数の項目（人気／満足／費用対効果／おススメ／価格）からランキング化</li>
			</ul>
		</div>
	</div>
</section>

<?php get_template_part( 'ranking' ); ?>

<section class="shohin-area">
	<div class="container">
	<div class="row">
	<?php
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'ranking', //カスタム投稿タイプ名
		'posts_per_page' => 8, //表示する記事数
		'meta_key' => 'ninki_r', // カスタムフィールドの項目名
		'orderby' => 'meta_value_num', // 数値として比較する
		'order' => 'ASC' // 昇順
	); ?>
	<?php $my_query = new WP_Query( $args ); ?>
	<?php if ( $my_query->have_posts() ) : ?>
		<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

		<div id="<?php the_field('syosai_link'); ?>" class="shohin-syosai">
			<h3><?php the_field('shohin_title'); ?></h3>
			<div class="col-md-3"><img src="<?php the_field('shohin_img'); ?>" class="img-responsive center-block" alt=""></div>
			<div class="col-md-9">
				<table class="table">
					<tbody>
					<tr>
						<td>
							<div class="col-sm-3">総合</div>						
							<div class="col-sm-5"><img src="<?php the_field('ninki_hyoka'); ?>" class="img-responsive center-block" alt=""/></div>
							<div class="col-sm-4"><?php the_field('ninki_v'); ?>／5点（第<?php the_field('ninki_r'); ?>位）</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="col-sm-3">満足度</div>						
							<div class="col-sm-5"><img src="<?php the_field('manzoku_hyoka'); ?>" class="img-responsive center-block" alt=""/></div>
							<div class="col-sm-4"><?php the_field('manzoku_v'); ?>／5点（第<?php the_field('manzoku_r'); ?>位）</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="col-sm-3">費用対効果度</div>
							<div class="col-sm-5"><img src="<?php the_field('hiyotaikouka_hyoka'); ?>" class="img-responsive center-block" alt=""/></div>
							<div class="col-sm-4"><?php the_field('hiyotaikouka_v'); ?>／5点（第<?php the_field('hiyotaikouka_r'); ?>位）</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="col-sm-3">おススメ度</div>
							<div class="col-sm-5"><img src="<?php the_field('osusume_hyoka'); ?>" class="img-responsive center-block" alt=""/></div>
							<div class="col-sm-4"><?php the_field('osusume_v'); ?>／5点（第<?php the_field('osusume_r'); ?>位）</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="col-sm-3">価格</div>
							<div class="col-sm-5"><?php the_field('price_v'); ?>円</div>
							<div class="col-sm-4">（第<?php the_field('price_r'); ?>位）</div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-12">
				<div class="tokucho-title">「<?php the_field('shohin_title'); ?>」の特徴</div>
				<div class="tokucho-txt">
					<?php the_field('rankbox_TXT'); ?>
				</div>
				<div class="btn_af">
					<a onclick="return gtag_report_conversion('<?php echo esc_url( home_url( '/' ) ); ?>')" class="btn btn-danger" href="<?php the_field('af_link'); ?>" role="button"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_af-link.png" class="img-responsive center-block" alt=""/></a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	</div>
	</div>
</section>
	
<?php get_footer(); ?>
