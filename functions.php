<?php

/** ウィジェットエリア **/
function sidebar_widgets_init() {
register_sidebar(array(
  'name'          => 'サイドバー',
  'id'            => 'sidebar',
  'description'   => 'サイドバーに表示されるウィジェットエリアです。',
  'before_widget' => '<div class="side-content-list">',
  'after_widget'  => '</div>',
  'before_title'  => '',
  'after_title'   => '',
));
}
add_action( 'widgets_init', 'sidebar_widgets_init' );

function footerl_widgets_init() {
register_sidebar(array(
  'name'          => 'フッター左',
  'id'            => 'footer-left',
  'description'   => 'フッターの左側に表示されるウィジェットです。',
  'before_widget' => '<div>',
  'after_widget'  => '</div>',
  'before_title'  => '<h3>',
  'after_title'   => '</h3>'
));
}
add_action( 'widgets_init', 'footerl_widgets_init' );

function footerc_widgets_init() {
register_sidebar(array(
  'name'          => 'フッター中',
  'id'            => 'footer-center',
  'description'   => 'フッターの真ん中に表示されるウィジェットです。',
  'before_widget' => '<div>',
  'after_widget'  => '</div>',
  'before_title'  => '<h3>',
  'after_title'   => '</h3>'
));
}
add_action( 'widgets_init', 'footerc_widgets_init' );

function footerr_widgets_init() {
register_sidebar(array(
  'name'          => 'フッター右',
  'id'            => 'footer-right',
  'description'   => 'フッターの右側に表示されるウィジェットです。',
  'before_widget' => '<div>',
  'after_widget'  => '</div>',
  'before_title'  => '<h3>',
  'after_title'   => '</h3>'
));
}
add_action( 'widgets_init', 'footerr_widgets_init' );

/** スマホ表示の条件分岐用 **/
function is_mobile() {
    $useragents = array(
        'iPhone', // iPhone
        'iPod', // iPod touch
        'Android.*Mobile', // 1.5+ Android *** Only mobile
        'Windows.*Phone', // *** Windows Phone
        'dream', // Pre 1.5 Android
        'CUPCAKE', // 1.5+ Android
        'blackberry9500', // Storm
        'blackberry9530', // Storm
        'blackberry9520', // Storm v2
        'blackberry9550', // Storm v2
        'blackberry9800', // Torch
        'webOS', // Palm Pre Experimental
        'incognito', // Other iPhone browser
        'webmate' // Other iPhone browser
	);
    $pattern = '/'.implode('|', $useragents).'/i';
    return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

/** 抜粋 **/
function my_excerpt_more($post) {
	return '…<a class="read-more" href="'. get_permalink( get_the_ID() ) . '">続きを読む</a>';
}
add_filter('excerpt_more', 'my_excerpt_more');