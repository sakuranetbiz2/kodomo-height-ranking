</div><!--/main-->

<footer>
	<div class="container-fluid">
	<div class="row">
		<div class="footer-area">
		<!--フッター左-->
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer-left' ); ?>
			</div>
		<!--フッター中-->
			<div class="col-md-4">
				<ul>
				<?php
				$args = array(
					'post_status' => 'publish',
					'post_type' => 'ranking', //カスタム投稿タイプ名
					'posts_per_page' => -1, //表示する記事数
					'meta_key' => 'ninki_r', // カスタムフィールドの項目名
					'orderby' => 'meta_value_num', // 数値として比較する
					'order' => 'ASC' // 昇順
				); ?>
				<?php $my_query = new WP_Query( $args ); ?>
				<?php if ( $my_query->have_posts() ) : ?>
					<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
						<li><a href="<?php echo esc_url( home_url() ); ?>#<?php the_field('syosai_link'); ?>"><?php the_field('shohin_title'); ?></a></li>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
				
				<?php dynamic_sidebar( 'footer-center' ); ?>
			</div>
		<!--フッター右-->
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer-right' ); ?>
			</div>
				<div class="clearfix"></div>
			<p>Copyright 2018 <?php bloginfo('name'); ?> All Rights Reserved.</p>
		</div>
	</div>
	</div>
		<?php if ( is_home() || is_front_page() ) : ?>
			<p id="topbutton"><a href="#ranking">▲ランキングへ戻る</a></p>
		<?php else : ?>
			<p id="topbutton"><a href="#top">▲TOPへ戻る</a></p>
		<?php endif; ?>
</footer>
	
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.metadata.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.tablesorter.min.js"></script>
<script>
 $('#rankSort').tablesorter();
 $('.sort').on('change', function() {
 $('#rankSort').tablesorter({
	  sortList: [ this.value ? this.value.split(',') : [0, 0] ],
  });
 });
</script>
<script>
$(function(){
    $('a[href^=#]').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});
</script>
<script type="text/javascript">
   $(function() {
      $(window).scroll(function () {
         var TargetPos = 350;
         var ScrollPos = $(window).scrollTop();
         if( ScrollPos >= TargetPos) {
            $("#topbutton").fadeIn();
         }
         else {
            $("#topbutton").fadeOut();
         }
      });
   });
</script>

<?php wp_footer(); ?>
</body>
</html>
