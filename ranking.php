<section id="ranking" class="ranking-area">
	<div class="container">
			<h2><img src="<?php echo get_template_directory_uri(); ?>/images/ranking-area_title.png" class="img-responsive center-block" alt="厳選「子供の成長サプリメント」人気ランキング"/></h2>
	<div class="row">
		<div class="sort-btn-area">
			<p class="text-blue"><i class="fas fa-sort-amount-down fa-2x"></i>ボタンを押すと表の並び替えができます。</p>
			<div class="col-xs-4 col-md-2 sort-btn">
				<input type="radio" id="rb1" name="sort" class="sort" value="2,0" checked><label for="rb1" class="sort-btn">
					<?php if ( is_mobile() ) : ?>
						<th>総合</th>
					<?php else: ?>
						<th>総合ランキング</th>
					<?php endif; ?>
				</label>
				<p>満足&#x002F;費用対効果&#x002F;おススメの総合評価</p>
			</div>
			<div class="col-xs-4 col-md-2 sort-btn">
				<input type="radio" id="rb2" name="sort" class="sort" value="3,0"><label for="rb2">満足度<?php if ( is_mobile() ) : ?><br><?php else: ?><?php endif; ?><span class="s-text">が高い順</span></label>
				<p>利用後の満足</p>
			</div>
			<div class="col-xs-4 col-md-2 sort-btn">
				<input type="radio" id="rb3" name="sort" class="sort" value="4,0"><label for="rb3">費用対効果<?php if ( is_mobile() ) : ?><br><?php else: ?><?php endif; ?><span class="s-text">が高い順</span></label>
				<p>費用に対しての効果</p>
			</div>
			<div class="col-xs-4 col-md-2 sort-btn">
				<input type="radio" id="rb4" name="sort" class="sort" value="5,0"><label for="rb4">おススメ度<?php if ( is_mobile() ) : ?><br><?php else: ?><?php endif; ?><span class="s-text">が高い順</span></label>
				<p>友人にもおススメできる</p>
			</div>
			<div class="col-xs-4 col-md-2 sort-btn">
				<input type="radio" id="rb5" name="sort" class="sort" value="6,0"><label for="rb5">価格<?php if ( is_mobile() ) : ?><br><?php else: ?><?php endif; ?><span class="s-text">が安い順</span></label>
				<p>月額の価格</p>
			</div>
		</div>
		<ul>
</ul>
	<div class="ranking-table">
			<table id="rankSort" class="table table-bordered">
				<thead>
					<tr>
						<th class="rank"></th>
						<th></th>
						<th class="{sorter:'metadata'}"></th>
						<th class="{sorter:'metadata'}"></th>
						<th class="{sorter:'metadata'}"></th>
						<th class="{sorter:'metadata'}"></th>
						<th class="{sorter:'metadata'}"></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th class="rank">順位</th>
						<?php if ( is_mobile() ) : ?><th>商品</th><?php else: ?><th>商品名</th><?php endif; ?>
						<th>総合</th>
						<th>満足度</th>
						<th>費用対効果度</th>
						<th>おススメ度</th>
						<?php if ( is_mobile() ) : ?><th>安さ</th><?php else: ?><th>月額価格</th><?php endif; ?>
						<th class="rank-syosai">詳細</th>
					</tr>

	<?php
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'ranking', //カスタム投稿タイプ名
		'posts_per_page' => 8, //表示する記事数
	); ?>
	<?php $my_query = new WP_Query( $args ); ?>
	<?php if ( $my_query->have_posts() ) : ?>
		<?php $x=1; while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
					<tr>
						<td class="rank"></td>
						<td class="shohin-s">
							<?php if ( is_mobile() ) : ?>
							<?php else: ?>
							<?php the_field('shohin_title'); ?>
							<?php endif; ?>
							<img src="<?php the_field('shohin_img'); ?>" alt=""/>
						</td>
						<td class="{sortValue:<?php the_field('ninki_r'); ?>} rank-star">
							<?php if ( is_mobile() ) : ?>
							<?php else: ?>
								<img src="<?php the_field('ninki_star'); ?>" alt=""/>
							<?php endif; ?>
							<?php the_field('ninki_v'); ?>
						</td>
						<td class="{sortValue:<?php the_field('manzoku_r'); ?>} rank-star">
							<?php if ( is_mobile() ) : ?>
							<?php else: ?>
							<img src="<?php the_field('manzoku_star'); ?>" alt=""/>
							<?php endif; ?>
							<?php the_field('manzoku_v'); ?>
						</td>
						<td class="{sortValue:<?php the_field('hiyotaikouka_r'); ?>} rank-star">
							<?php if ( is_mobile() ) : ?>
							<?php else: ?>
							<img src="<?php the_field('hiyotaikouka_star'); ?>" alt=""/>
							<?php endif; ?>
							<?php the_field('hiyotaikouka_v'); ?>
						</td>
						<td class="{sortValue:<?php the_field('osusume_r'); ?>} rank-star">
							<?php if ( is_mobile() ) : ?>
							<?php else: ?>
							<img src="<?php the_field('osusume_star'); ?>" alt=""/>
							<?php endif; ?>
							<?php the_field('osusume_v'); ?>
						</td>
						<td class="{sortValue:<?php the_field('price_r'); ?>} t-price">
							<?php if ( is_mobile() ) : ?>
								<?php the_field('price_r'); ?>
							<?php else: ?>
								<?php the_field('price_v'); ?>円
							<?php endif; ?>
						</td>
						<td class="rank-syosai">
							<a href="#<?php the_field('syosai_link'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/rank-btn_syosai.png" class="img-responsive center-block" alt=""/></a>
						</td>
					</tr>
		<?php $x++; endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</section>