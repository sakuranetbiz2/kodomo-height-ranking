<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
  	<div class="container">
		<div class="row">
			<div class="single-content col-md-9">
				<h2><?php the_title(); ?></h2>
				<div class="content-area">
					<?php the_content(); ?>
				</div>
			</div>
  <?php endwhile; ?>
<?php endif; ?>

<?php get_sidebar(); ?>
			
		</div>
	</div>

<?php get_footer(); ?>