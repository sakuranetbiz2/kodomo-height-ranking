<div class="sidebar col-md-3">
	<h3><img src="<?php echo get_template_directory_uri(); ?>/images/side_rank-title.<?php if ( wp_is_mobile() ) : ?>png<?php else: ?>jpg<?php endif; ?>" alt="子供の成長サプリ人気ランキング"/></h3>
	<div class="side-waku">
<?php
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'ranking', //カスタム投稿タイプ名
		'posts_per_page' => 5, //表示する記事数
		'meta_key' => 'ninki_r', // カスタムフィールドの項目名
		'orderby' => 'meta_value_num', // 数値として比較する
		'order' => 'ASC' // 昇順
	); ?>
	<?php $my_query = new WP_Query( $args ); ?>
	<?php if ( $my_query->have_posts() ) : ?>
		<?php $x=1; while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
	
	<div class="side-rank-shohin">
		<span class="side-rank-shohin-title"><img src="<?php echo get_template_directory_uri(); ?>/images/rank-<?php echo $x; ?>.png" alt=""/><?php the_field('shohin_title'); ?></span>
		<img src="<?php the_field('shohin_img'); ?>" class="side-shohin-img img-responsive center-block" alt="<?php the_field('shohin_title'); ?>">
		<p><a href="<?php echo esc_url( home_url() ); ?>#<?php the_field('syosai_link'); ?>"><span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span>&nbsp;詳細はこちら</a></p>
	</div>

		<?php $x++; endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>

		<div><a class="btn btn-danger btn-sm" href="<?php echo esc_url( home_url() ); ?>#rankSort">ランキングの続きはこちら</a></div>
	</div>
	
	<h3><img src="<?php echo get_template_directory_uri(); ?>/images/side_column-title.<?php if ( wp_is_mobile() ) : ?>png<?php else: ?>jpg<?php endif; ?>" alt=""/></h3>
	<?php dynamic_sidebar( 'sidebar' ); ?>

</div>
